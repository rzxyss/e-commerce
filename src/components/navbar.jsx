import React, { useState } from "react";
import {BsCart2} from 'react-icons/bs'
import {AiOutlineMenu} from 'react-icons/ai'

export default function Navbar() {
  const [nav, setNav] = useState(false)

  return (
    <div className="p-3 bg-[#F8F8F8]">
      <div className="flex flex-col lg:flex-row lg:justify-between lg:items-center container mx-auto gap-5">
        <div className="flex justify-between items-center">
          <h1 className="font-roboto font-bold text-3xl">E-Commerce</h1>
          <button className="h-full bg-[#393D45] p-4 lg:hidden" onClick={() => setNav(!nav)}>
            <AiOutlineMenu className="fill-white" />
          </button>
        </div>
        <div className={`flex items-start lg:items-center flex-col lg:flex-row gap-5 ${nav ? 'flex' : 'lg:flex hidden'}`}>
          <button className="border-b-[1px] border-[#393D45]/80">
            <a href="/" className="font-roboto font-bold text-base">
              Home
            </a>
          </button>
          <h1 className="hidden lg:flex">/</h1>
          <button>
            <a href="/" className="font-roboto font-normal text-base">
              Product
            </a>
          </button>
          <h1 className="hidden lg:flex">/</h1>
          <button>
            <a href="/" className="font-roboto font-normal text-base">
              About
            </a>
          </button>
        </div>
        <div className={`flex items-center gap-5 ${nav ? 'flex' : 'lg:flex hidden'}`}>
          <div>
            <BsCart2 className="fill-[#616467] w-7 h-7" />
          </div>
          <button className="bg-[#211F1C] p-3">
            <h1 className="text-white">LOGIN</h1>
          </button>
        </div>
      </div>
    </div>
  );
}
